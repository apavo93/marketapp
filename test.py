import os
from app import app, models
import unittest


TEST_DB = "test.db"

class FlaskTest(unittest.TestCase):
    ############################
    #### setup and teardown ####
    ############################
 
    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + \
            os.path.join(os.getcwd(), TEST_DB)
        self.app = app.test_client()
        models.db.drop_all()
        models.db.create_all()
    
    # executed after each test
    def tearDown(self):
        pass

    ###############
    #### tests ####
    ###############
    def test_fill_booktable(self):
        response = self.app.get('/_booksdb', follow_redirects = True)
        books = models.Books.query.all()
        self.assertNotEqual(len(books),0)
        self.assertEqual(response.status_code, 200)

    def test_main_page(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
    
    def test_purchase(self):
        self.app.get('/_booksdb', follow_redirects = True)
        self.app.post("/",data=dict(book_id=1, submit = True))
        self.app.post("/",data=dict(book_id=2, submit = True))
        response = self.app.post("/",data=dict(book_id=3, submit = True))
        purchase = models.Purchase.query.all()
        purchase_len = len(purchase[0].books)
        self.assertNotEqual(len(purchase),0)
        self.assertEqual(len(purchase[0].books),3)
        self.assertEqual(response.status_code, 200)
        self.app.post("/purchase",data = dict(book_id=2, submit = True))
        purchase = models.Purchase.query.all()
        self.assertNotEqual(purchase_len,len(purchase[0].books))
        response = self.app.get("/deleteAll", follow_redirects = True)
        purchase = models.Purchase.query.all()
        self.assertEqual(len(purchase),0)


 
 
if __name__ == "__main__":
    unittest.main()
