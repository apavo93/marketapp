from flask_wtf import FlaskForm
from wtforms import SelectField, IntegerField, HiddenField, SubmitField

class PurchaseForm(FlaskForm):
    book_id = HiddenField("bookid")
    submit = SubmitField(label = "Add")

    