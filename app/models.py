from app import app
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy(app)

class Books(db.Model):

    __tablename__ = 'BOOKS'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String)
    image = db.Column(db.String)
    purchase_id = db.Column(db.Integer, db.ForeignKey('Purchase.id'),
        nullable=True)

class Purchase(db.Model):
    
    __tablename__ = 'Purchase'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    books = db.relationship('Books', lazy=False)

db.create_all()