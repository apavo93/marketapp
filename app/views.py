import os

from flask import render_template, flash, redirect, session, request
from app import app, models
from .forms import *



@app.route('/', methods=['GET','POST'])
def index():
    """
    Show and allows to add items to purchase
    """
    form = PurchaseForm()
    error = ""
    try:
        if form.validate_on_submit():
            book = models.Books.query.filter_by(id=str(form.book_id.data)).first()
            #Check if there is a purchase created in this session
            if "purchase" not in session:
                purchase = models.Purchase()
            else:
                purchase =  models.Purchase.query.filter_by(id= session['purchase']).first()

            purchase.books.append(book)
            models.db.session.add(purchase)
            models.db.session.commit()
            session['purchase'] = purchase.id
        books = models.Books.query.all()
    except:
        books = []
        error = "Error loading books"
    return render_template("index.html",
                            books = books,
                            error = error,
                            form=form)

@app.route("/purchase", methods=["GET","POST"])
def purchase():
    """
    Show and remove items from a purchase
    """
    form = PurchaseForm()
    msg = ""
    try:
        if "purchase" in session:
            purchase =  models.Purchase.query.filter_by(id= session['purchase']).first()
            if form.validate_on_submit():
                for book in purchase.books:
                    if book.id == int(form.book_id.data):
                        del purchase.books[purchase.books.index(book)]
                        models.db.session.commit()
                        break
            books = purchase.books
        else:
            msg = "No items in purchase"
            books = []
    except:
        books = []
        msg = "Error in page"
    return render_template("purchase.html",
                            books = books,
                            msg = msg,
                            form = form)


@app.route("/deleteAll", methods=["GET"])
def  delete():
    """
    Delete purchase session
    """
    try:
        purchase =  models.Purchase.query.filter_by(id= session['purchase']).first()
        models.db.session.delete(purchase)
        models.db.session.commit()
        session.clear()
    except Exception,e:
        print "Error in delete all {}".format(str(e))
    finally:
        return redirect("/purchase")        


@app.route('/_booksdb', methods=['GET'])
def _db():
    """
    Create entries in book table
    """
    for image in os.listdir(os.path.join(os.getcwd(),"app/static//images")):
        book = models.Books(title = image.split(".")[0], image = image )
        models.db.session.add(book)
    models.db.session.commit()
    return redirect("/")
    